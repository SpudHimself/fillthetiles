﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class UnityTips : EditorWindow
{
    public int toolBar;

    void OnEnable()
    {
        toolBar = EditorPrefs.GetInt("index", 0);
    }

    void OnDisable()
    {
        EditorPrefs.SetInt("index", toolBar);
    }

    [MenuItem("Tools/Unity Tips")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(UnityTips));
    }

    public static void Init()
    {
        UnityTips myWindow = (UnityTips)EditorWindow.GetWindow(typeof(UnityTips));
        myWindow.Show();
        myWindow.autoRepaintOnSceneChange = true;
        EditorApplication.modifierKeysChanged += myWindow.Repaint;
    }

    void OnGUI()
    {
        UnityTips myWindow = (UnityTips)EditorWindow.GetWindow(typeof(UnityTips));

        //Put some icons under 'Assets/Editor Default Resources/Icons'
        Texture[] images = new Texture[5];
        images[0] = EditorGUIUtility.Load("Icons/BM.png") as Texture2D;
        images[1] = EditorGUIUtility.Load("Icons/seahorse_colored.png") as Texture2D;
        images[2] = EditorGUIUtility.Load("Icons/starfish_colored.png") as Texture2D;
        images[3] = EditorGUIUtility.Load("Icons/octopus_colored.png") as Texture2D;
        images[4] = EditorGUIUtility.Load("Icons/lobster_colored.png") as Texture2D;

        toolBar = GUILayout.Toolbar(toolBar, images, GUILayout.MinWidth(223), GUILayout.MaxWidth(223), GUILayout.MinHeight(20), GUILayout.MaxHeight(20));

        switch (toolBar)
        {
            case 0:
                //Do stuff;
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
        }
    }
}
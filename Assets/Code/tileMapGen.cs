﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tileMapGen : MonoBehaviour
{
    public GameObject tilePrefab;
    public Vector2 gridSize;
    public GameObject[,] grid;
    public float spacing;
    public bool startSpawned, endSpawned;



    // Use this for initialization
    void Start()
    {
        //doesnt fucking work since you cant access colliders from a prefab
        //spacing = tilePrefab.GetComponent<BoxCollider2D>().bounds.extents.x;
        spacing = 5.0f; //lets just hardcode this fucker

        //make map the size of the grid
        grid = new GameObject[(int)gridSize.x, (int)gridSize.y];

        //moved grid gen to its own function so we can reuse it (eg buttons)
        generateGrid();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            deleteGrid();
            generateGrid();
            gameManager.Instance.hasWon = false;
        }

        checkTilesForEmpty();

        //if (returnAllTiles() == tileBehaviour.tileType.Empty  /*grid doesnt return any empty tiles (clicked them all)*/)
        //{
        //    gameManager.Instance.clickedThemAll = true;
        //}
    }

    void generateGrid()
    {
        //generate the grid!
        for (int x = 0; x < gridSize.x; x++)
        {
            for (int y = 0; y < gridSize.y; y++)
            {
                //assign and instantiate the prefab to the grid, setting the parent to this object as well
                grid[x, y] = Instantiate(tilePrefab, new Vector3(x * spacing, -(y * spacing)), tilePrefab.transform.rotation) as GameObject;
                grid[x, y].transform.SetParent(gameObject.transform);

                //time to fuck with the tile properties
                tileBehaviour currentMapTile = grid[x, y].GetComponent<tileBehaviour>();
                currentMapTile.randomTile();

                //makes sure that only one start and end gets spawned whilst still being random
                if (currentMapTile.getTile() == tileBehaviour.tileType.Start && startSpawned == false)
                {
                    startSpawned = true;
                }
                else if (currentMapTile.getTile() == tileBehaviour.tileType.End && endSpawned == false)
                {
                    endSpawned = true;
                }
                else
                {
                    while (currentMapTile.getTile() != tileBehaviour.tileType.Empty)
                    {
                        //currentMapTile.randomTile();
                        currentMapTile.setTile(tileBehaviour.tileType.Empty);
                    }
                }
            }
        }
        getStartTile();
        getEndTile();
    }

    void deleteGrid()
    {
        foreach (GameObject GO in grid)
        {
            Destroy(GO);
            startSpawned = false;
            endSpawned = false;
        }

    }

    tileBehaviour.tileType[] returnAllTiles()
    {
        //we're basically converting the 2d grid array to a 1d array for easier handling
        tileBehaviour.tileType[] returnArray = new tileBehaviour.tileType[grid.Length];

        for (int x = 0; x < gridSize.x; x++)
        {
            for (int y = 0; y < gridSize.y; y++)
            {
                for (int count = 0; count < grid.Length; count++)
                {
                    tileBehaviour currentMapTile = grid[x, y].GetComponent<tileBehaviour>();
                    returnArray[count] = currentMapTile.getTile();
                }
            }
        }


        return returnArray;
    }

    public bool checkTilesForEmpty()
    {
        foreach (GameObject TB in grid)
        {
            if (TB.GetComponent<tileBehaviour>().getTile() == tileBehaviour.tileType.Empty)
            {
                return gameManager.Instance.clickedThemAll = false;
            }
        }
        return gameManager.Instance.clickedThemAll = true;

    }

    public tileBehaviour getStartTile()
    {
        foreach (GameObject TB in grid)
        {
            if (TB.GetComponent<tileBehaviour>().getTile() == tileBehaviour.tileType.Start)
            {
                return gameManager.Instance.startTile = TB.GetComponent<tileBehaviour>();
            }
        }
        return null;
    }

    public tileBehaviour getEndTile()
    {
        foreach (GameObject TB in grid)
        {
            if (TB.GetComponent<tileBehaviour>().getTile() == tileBehaviour.tileType.End)
            {
                return gameManager.Instance.endTile = TB.GetComponent<tileBehaviour>();
            }
        }
        return null;
    }

}




﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class tileBehaviour : MonoBehaviour
{
    public enum tileType
    {
        Start,
        End,
        Empty,
        Flipped,
        Block
    }

    //logic
    public tileType tile;
    public bool isClicked;

    //rendering
    public SpriteRenderer _renderer;
    public Sprite[] sprites;

    // Use this for initialization
    void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        sprites = Resources.LoadAll<Sprite>("tileSheet");
    }

    // Update is called once per frame
    void Update()
    {
        gameManager.Instance.isHoldingDownClick = Input.GetMouseButton(0);

        if (gameManager.Instance.clickedThemAll)
        {
            Debug.Log("weewooweewoo");
        }

        if (!gameManager.Instance.isHoldingDownClick)
        {
            gameManager.Instance.firstClickStart = false;
        }

        if (!gameManager.Instance.hasWon)
        {
            gameManager.Instance.winText.SetActive(false);

            switch (tile)
            {
                case tileType.Start:
                    _renderer.sprite = sprites[0];

                    if (isClicked) //because we only check on mouse over, this will be true until we click on it again
                    {
                        gameManager.Instance.firstClickStart = true;
                    }

                    if (!gameManager.Instance.isHoldingDownClick)
                    {
                        gameManager.Instance.firstClickStart = false;
                        //gameManager.Instance.startTile.GetComponent<tileBehaviour>().isClicked = false;
                        gameManager.Instance.startTile.isClicked = false;
                    }
                    break;

                case tileType.End:
                    _renderer.sprite = sprites[1];

                    if (isClicked && gameManager.Instance.clickedThemAll)
                    {
                        gameManager.Instance.hasWon = true;
                    }
                    break;

                case tileType.Empty:
                    _renderer.sprite = sprites[2];

                    if (isClicked && gameManager.Instance.isHoldingDownClick && gameManager.Instance.firstClickStart)
                    {
                        tile = tileType.Flipped;
                    }
                    break;

                case tileType.Flipped:
                    _renderer.sprite = sprites[3];

                    if (isClicked)
                    {

                    }

                    if (!gameManager.Instance.isHoldingDownClick)
                    {
                        isClicked = false;
                        unFlip();
                    }
                    break;

                case tileType.Block:
                    _renderer.sprite = sprites[4];

                    if (isClicked)
                    {

                    }
                    break;

                default:
                    Debug.logger.Log("aw fook, somethings not right!");
                    Debug.Break();
                    break;
            }

        }
        else
        {
            //player has won
            gameManager.Instance.winText.SetActive(true);
            Debug.logger.Log("ayyy u win");
        }

        


    }

    public tileType getTile()
    {
        return tile;
    }

    public void setTile(tileType tileToSet)
    {
        tile = tileToSet;
    }

    public tileType randomTile()
    {
        return tile = (tileType)UnityEngine.Random.Range(0, Enum.GetValues(typeof(tileType)).Length);
    }

    public tileType unFlip()
    {
        return tile = tileType.Empty;
    }

    private void OnMouseOver()
    {
        //gotta hold click inside the tile
        if (Input.GetMouseButton(0) && getTile() == tileType.Start)
        {
            isClicked = true;
        }
        else if (Input.GetMouseButton(0) && gameManager.Instance.firstClickStart)
        {
            isClicked = true;
        }
        else
        {
            isClicked = false;
        }
    }

    //private void OnMouseDown()
    //{
    //    Debug.logger.Log("click!");
    //}
}

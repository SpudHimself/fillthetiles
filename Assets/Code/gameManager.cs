﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameManager : Singleton<gameManager>
{

    protected gameManager() { } // guarantee this will be always a singleton only - can't use the constructor!

    public bool isHoldingDownClick = false;
    public bool firstClickStart = false;
    public bool clickedThemAll = false;
    public tileBehaviour startTile, endTile;
    public bool hasWon = false;
    public GameObject winText;

    // Use this for initialization
    void Start()
    {
        winText = GameObject.Find("WinText");
        winText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.logger.Log("Holding Down Click: ", isHoldingDownClick);
        Debug.logger.Log("First Click Start: ", firstClickStart);
        Debug.logger.Log("clickedThemAll: ", clickedThemAll);
    }


}
